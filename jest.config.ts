import type {Config} from 'jest';

const config: Config = {
  verbose: false,
  extensionsToTreatAsEsm: ['.ts'],
  errorOnDeprecated: true,
};

export default config;