import { endianness } from 'os';
import { emitWarning } from 'process';
import { encode as encodeqp } from 'libqp';
function extendBuff(buff, size, fill, encoding) {
    let temp = Buffer.alloc(buff.length + size, fill, encoding);
    buff.copy(temp);
    return temp;
}
function unknownWarn(format, formatStr, result) {
}
// https://apidock.com/ruby/Array/pack
// https://perldoc.perl.org/functions/pack
// https://perldoc.perl.org/perlpacktut
export function pack(value, formatStr) {
    const nativeEndian = endianness();
    let valueArr = [];
    let formats = formatStr.replace(/ /g, '').split(/([A-JL-NPQSUVXZa-jl-npqsuvwx@](?:_|!?(?:>|<)?)?(?:\d+|\*)?)/).filter(v => v);
    let vpos = 0;
    let rpos = 0;
    if (typeof value[0] == 'string') {
        valueArr.push(...value.map(v => Buffer.from(v).toJSON().data));
    }
    else {
        valueArr.push(...Buffer.from(value));
    }
    let valueBuff = Buffer.from(valueArr.flat());
    let result = Buffer.alloc(0);
    formats.forEach((format, i) => {
        var _a, _b, _c, _d;
        if ((format.includes('<') || format.includes('>')) && !'sSiIlLqQjJ'.includes(format[0])) {
            throw new Error(`Argument error: '${(_a = format.match(/(<|>)/)) === null || _a === void 0 ? void 0 : _a.at(0)}' allowed only after types sSiIlLqQjJ in '${format}'`);
        }
        let endian = nativeEndian;
        if (format.includes('<'))
            endian = 'LE';
        if (format.includes('>'))
            endian = 'BE';
        let num;
        let numMatch = (_b = format.match(/(\d+|\*)$/)) === null || _b === void 0 ? void 0 : _b.at(1);
        if (numMatch == '*') {
            num = valueBuff.length / formats.length;
        }
        else if (numMatch == undefined) {
            num = 1;
            // } else if (formats.length == valueBuff.length) {
            //     num = 1;
        }
        else {
            num = Number(numMatch) || 1;
        }
        let end = 'AaZmM'.includes(format[0]) ? vpos + 1 : vpos + num;
        result = extendBuff(result, num);
        let subValue = valueBuff.subarray(vpos, end);
        switch (format[0]) {
            case 'C':
                subValue.forEach(val => rpos = result.writeUint8(val, rpos));
                break;
            case 'S':
                subValue.forEach(val => {
                    result = extendBuff(result, 1); // character at rpos needs two bytes
                    rpos = result[`writeUInt16${endian}`](val, rpos);
                });
                break;
            case 'I':
                subValue.forEach(val => {
                    result = extendBuff(result, 3); // character at rpos needs two bytes
                    rpos = result[`writeUInt${endian}`](val, rpos, 4);
                });
                break;
            case 'L':
                subValue.forEach(val => {
                    if ('!_'.includes(format[1])) { // unsigned short, native endian
                        result = extendBuff(result, 7); // character at rpos needs eight bytes
                        rpos = result[`writeBigUInt64${endian}`](BigInt(val), rpos);
                    }
                    else { // 16-bit unsigned, native endian (uint16_t)
                        result = extendBuff(result, 3); // character at rpos needs four bytes
                        rpos = result[`writeUInt32${endian}`](val, rpos);
                    }
                });
                break;
            case 'Q':
                subValue.forEach(val => {
                    result = extendBuff(result, 7); // character at rpos needs eight bytes
                    rpos = result[`writeBigUInt64${endian}`](BigInt(val), rpos);
                });
                break;
            // case 'J': subValue.forEach(val => rpos = result[`writeUInt${endian}`](val, rpos, 1)); break;
            case 'c':
                subValue.forEach(val => rpos = result.writeInt8(val, rpos));
                break;
            case 's':
                subValue.forEach(val => {
                    result = extendBuff(result, 1); // character at rpos needs two bytes
                    rpos = result[`writeInt16${endian}`](val, rpos);
                });
                break;
            case 'l':
                subValue.forEach(val => {
                    if ('!_'.includes(format[1])) { // unsigned short, native endian
                        result = extendBuff(result, 7); // character at rpos needs eight bytes
                        rpos = result[`writeBigInt64${endian}`](BigInt(val), rpos);
                    }
                    else { // 16-bit unsigned, native endian (uint16_t)
                        result = extendBuff(result, 3); // character at rpos needs four bytes
                        rpos = result[`writeInt32${endian}`](val, rpos);
                    }
                });
                break;
            case 'q':
                subValue.forEach(val => {
                    result = extendBuff(result, 7); // character at rpos needs eight bytes
                    rpos = result[`writeBigInt64${endian}`](BigInt(val), rpos);
                });
                break;
            // case 'j': subValue.forEach(val => rpos = result[`writeInt${endian}`](val, rpos, 1)); break;
            case 'i':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result[`writeInt${endian}`](val, rpos, 4);
                });
                break;
            case 'n':
                subValue.forEach(val => {
                    result = extendBuff(result, 1);
                    rpos = result.writeUInt16BE(val, rpos);
                });
                break;
            case 'N':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result.writeUInt32BE(val, rpos);
                });
                break;
            case 'v':
                subValue.forEach(val => {
                    result = extendBuff(result, 1);
                    rpos = result.writeUInt16LE(val, rpos);
                });
                break;
            case 'V':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result.writeUInt32LE(val, rpos);
                });
                break;
            case 'U':
                subValue.forEach(val => {
                    rpos += Buffer.from([val]).copy(result, rpos);
                    // rpos = result.write(Buffer.from([val]).toString('utf-8'), rpos, 'utf8')
                });
                break;
            // case 'w': break;
            case 'D':
            case 'd':
                subValue.forEach(val => {
                    result = extendBuff(result, 7);
                    rpos = result[`writeDouble${endian}`](val, rpos);
                });
                break;
            case 'F':
            case 'f':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result[`writeFloat${endian}`](val, rpos);
                });
                break;
            case 'E':
                subValue.forEach(val => {
                    result = extendBuff(result, 7);
                    rpos = result.writeDoubleLE(val, rpos);
                });
                break;
            case 'e':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result.writeFloatLE(val, rpos);
                });
                break;
            case 'G':
                subValue.forEach(val => {
                    result = extendBuff(result, 7);
                    rpos = result.writeDoubleBE(val, rpos);
                });
                break;
            case 'g':
                subValue.forEach(val => {
                    result = extendBuff(result, 3);
                    rpos = result.writeFloatBE(val, rpos);
                });
                break;
            // val of subValue is a string
            case 'A': {
                let anum = numMatch === '*' ? 1 : num;
                // console.log(numMatch, num, anum);
                result = extendBuff(result, anum - 1, ' ', 'binary');
                result.write(subValue.toString('binary'), rpos, anum, 'binary');
                rpos = result.length;
                break;
            }
            case 'a': {
                let anum = numMatch === '*' ? 1 : num;
                // console.log(numMatch, num, anum);
                result = extendBuff(result, anum - 1, '\x00', 'binary');
                result.write(subValue.toString('binary'), rpos, anum, 'binary');
                rpos = result.length;
                break;
            }
            case 'Z': {
                let anum = numMatch === '*' ? 2 : num;
                // console.log(numMatch, num, anum);
                result = extendBuff(result, anum - 1, '\x00', 'binary');
                result.write(subValue.toString('binary'), rpos, anum, 'binary');
                rpos = result.length;
                // let buf = Buffer.alloc(numMatch == '*' ? 2 : num, '\x00', 'binary');
                // buf.write(subValue.toString());
                // rpos = rpos + result.write(buf.toString('binary'), rpos, buf.length, 'binary');
                break;
            }
            // case 'B': break;
            // case 'b': break;
            case 'H':
                rpos = result.write(Buffer.from(subValue.toString(), 'hex').toString());
                break;
            case 'h':
                rpos = result.write(Buffer.from(((_d = (_c = subValue.toString().match(/.{1,2}/g)) === null || _c === void 0 ? void 0 : _c.map(v => { var _a, _b; return (_b = (_a = v.split('')) === null || _a === void 0 ? void 0 : _a.reverse()) === null || _b === void 0 ? void 0 : _b.join(''); })) === null || _d === void 0 ? void 0 : _d.join('')) || subValue.toString(), 'hex').toString());
                break;
            // case 'u': break;
            case 'M':
                rpos = result.write(encodeqp(subValue) + '\n');
                break;
            case 'm': rpos = result.write(subValue.toString('base64') + '\n');
            // case 'P': break;
            // case 'p': break;
            case '@':
                rpos++;
                break;
            case 'X':
                rpos - num;
                break;
            case 'x':
                for (let i = 0; i < num; i++)
                    rpos = result.write('\0', rpos + i);
                break;
            default:
                {
                    emitWarning(`Unknown pack directive '${format}' in '${formatStr}'`);
                    subValue.forEach(val => {
                        result = result.subarray(0, result.length - 1);
                    });
                    // rpos += num;
                }
                ;
        }
        vpos += num;
    });
    if (rpos != result.length) {
        throw new Error('ArgumentError: Too few arguments');
    }
    return result.toString('binary');
}
