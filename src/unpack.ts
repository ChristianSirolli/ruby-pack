import { endianness } from 'os';
import { emitWarning } from 'process';
// https://apidock.com/ruby/v2_5_5/String/unpack
// https://perldoc.perl.org/functions/unpack
// https://perldoc.perl.org/perlpacktut
export function unpack(value: string, formatStr: string): number[] | string[] {
    const nativeEndian = endianness();
    let result: any[] = [];
    let formats = formatStr.replace(/ /g, '').split(/([A-JL-NPGSUVXZa-jl-npqsuvwx@](?:_|!?(?:>|<)?)?(?:\d|\*)?)/).filter(v => v);
    let pos = 0;
    let valueArr = value.split('');
    formats.forEach((format: string) => {
        if ((format.includes('<') || format.includes('>')) && !'sSiIlLqQjJ'.includes(format[0])) {
            throw new Error(`Argument error: '${format.match(/(<|>)/)?.at(0)}' allowed only after types sSiIlLqQjJ`);
        }
        let endian = nativeEndian;
        if (format.includes('<')) endian = 'LE';
        if (format.includes('>')) endian = 'BE';
        let length = Number(format.match(/(\d+|\*)$/)?.at(1)) || value.length;
        let valForBuff = valueArr.slice(pos, length).join('');
        pos += length - 1;
        switch (format[0]) {
            case 'C': result.push(Buffer.from(valForBuff).readUInt8()); pos += 1; break;
            case 'S': result.push(Buffer.from(valForBuff)[`readUInt16${endian}`]());  pos += 2; break;
            case 'L': {
                if ('!_'.includes(format[1])) {
                    result.push(Number(Buffer.from(valForBuff)[`readBigUInt64${endian}`]())); pos += 8;
                } else {
                    result.push(Buffer.from(valForBuff)[`readUInt32${endian}`]()); pos += 4;
                }
                break
            };
            case 'Q': result.push(Buffer.from(valForBuff)[`readBigUInt64${endian}`]()); pos += 8; break;
            // case 'J': result.push(Buffer.from(valForBuff)[`readUint${endian}`](0, Buffer.byteLength(valForBuff))); break;
            case 'c': result.push(Buffer.from(valForBuff).readInt8()); pos += 1; break;
            case 's': result.push(Buffer.from(valForBuff)[`readInt16${endian}`]()); pos += 2; break;
            case 'l': result.push(Buffer.from(valForBuff)[`readInt32${endian}`]()); pos += 4; break;
            case 'q': result.push(Number(Buffer.from(valForBuff)[`readBigInt64${endian}`]())); pos += 8; break;
            // case 'j': result.push(Buffer.from(valForBuff)[`readInt${endian}`](0, Buffer.byteLength(valForBuff))); break;
            case 'i': result.push(Buffer.from(valForBuff)[`readInt${endian}`](0, Buffer.byteLength(valForBuff))); pos += 1; break;
            case 'I': result.push(Buffer.from(valForBuff)[`readUInt${endian}`](0, Buffer.byteLength(valForBuff))); pos += 1; break;
            case 'n': result.push(Buffer.from(valForBuff).readUInt16BE()); pos += 1; break;
            case 'N': result.push(Buffer.from(valForBuff).readUInt32BE()); pos += 1; break;
            case 'v': result.push(Buffer.from(valForBuff).readUInt16LE()); pos += 1; break;
            case 'V': result.push(Buffer.from(valForBuff).readUInt32LE()); pos += 1; break;
            case 'U': result.push(...Buffer.from(valForBuff, 'utf-8')); pos += 1; break;
            // case 'w': result.push(valForBuff); break;
            case 'D': 
            case 'd': result.push(Number(Buffer.from(valForBuff)[`readFloat${endian}`]().toFixed(2))); pos += 1; break;
            case 'F':
            case 'f': result.push(Number(Buffer.from(valForBuff)[`readFloat${endian}`]().toFixed(1))); pos += 1; break;
            case 'E': result.push(Number(Buffer.from(valForBuff).readFloatLE().toFixed(2))); pos += 1; break;
            case 'e': result.push(Number(Buffer.from(valForBuff).readFloatLE().toFixed(1))); pos += 1; break;
            case 'G': result.push(Number(Buffer.from(valForBuff).readFloatBE().toFixed(2))); pos += 1; break;
            case 'g': result.push(Number(Buffer.from(valForBuff).readFloatBE().toFixed(1))); pos += 1; break;
            case 'A': result.push(Buffer.from(Buffer.from(valForBuff).filter((v, i, arr) => v != 0 && v != 32).buffer).toString('binary')); pos += 1; break;
            case 'a': result.push(Buffer.from(valForBuff).toString('binary')); pos += 1; break;
            // case 'Z': result.push(valForBuff); break;
            // case 'B': result.push(valForBuff); break;
            // case 'b': result.push(valForBuff); break;
            case 'H': result.push(Buffer.from(valForBuff).toString('hex')); pos += 1; break;
            case 'h': result.push(Buffer.from(valForBuff).toString('hex').match(/.{1,2}/g)?.map(v => v.split('')?.reverse()?.join(''))?.join('')); pos += 1; break;
            // case 'u': result.push(valForBuff); break;
            // case 'M': result.push(valForBuff); break;
            // case 'm': result.push(valForBuff); break;
            // case 'P': result.push(valForBuff); break;
            // case 'p': result.push(valForBuff); break;
            case '@': pos = 0; break;
            case 'X': pos--; break;
            case 'x': pos++; break;
            default: emitWarning(`Unknown pack directive '${format}' in '${formatStr}'`);
        }
    })
    return result;
}