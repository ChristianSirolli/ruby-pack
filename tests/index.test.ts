import {describe, expect, test} from '@jest/globals';
import { pack, unpack } from '../dist/index.js';
const abcIntArr = [65, 66, 67];
const abcStrArr = ["A", "B", "C"];
const unknownWarn = /Unknown pack directive '.*?' in '.*?'/;
const argErr = 'ArgumentError: Too few arguments';
describe('pack', () => {
    test('C', () => {
        expect(pack(abcIntArr, 'CCC')).toBe('ABC');
        expect(pack(abcIntArr, 'C3')).toBe('ABC');
        expect(pack(abcIntArr, 'C2C')).toBe('ABC');
        expect(pack(abcIntArr, 'CC2')).toBe('ABC');
        expect(pack(abcIntArr, 'C*')).toBe('ABC');
    });
    test('S', () => {
        expect(pack(abcIntArr, 'SSS')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S2S')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'SS2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S*')).toBe('A\x00B\x00C\x00');
    });
    test('L', () => {
        expect(pack(abcIntArr, 'LLL')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L3')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L2L')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'LL2')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
    });
    test('Q', () => {
        expect(pack(abcIntArr, 'QQQ')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q2Q')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'QQ2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('J', () => {
        expect(pack(abcIntArr, 'JJJ')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('c', () => {
        expect(pack(abcIntArr, 'ccc')).toBe('ABC');
        expect(pack(abcIntArr, 'c3')).toBe('ABC');
        expect(pack(abcIntArr, 'c2c')).toBe('ABC');
        expect(pack(abcIntArr, 'cc2')).toBe('ABC');
        expect(pack(abcIntArr, 'c*')).toBe('ABC');
    });
    test('s', () => {
        expect(pack(abcIntArr, 'sss')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's2s')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'ss2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's*')).toBe('A\x00B\x00C\x00');
    });
    test('l', () => {
        expect(pack(abcIntArr, 'lll')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l3')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l2l')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'll2')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
    });
    test('q', () => {
        expect(pack(abcIntArr, 'qqq')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q2q')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'qq2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('j', () => {
        expect(pack(abcIntArr, 'jjj')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('S! S_', () => {
        expect(pack(abcIntArr, 'S!S!S!')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S!3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S!2S!')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S!S!2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S!*')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S_S_S_')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S_3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S_2S_')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S_S_2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S_*')).toBe('A\x00B\x00C\x00');
    });
    test('L! L_', () => {
        expect(pack(abcIntArr, 'L!L!L!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L!3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L!2L!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L!L!2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L!*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L_L_L_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L_3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L_2L_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L_L_2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'L_*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('I I! I_', () => {
        expect(pack(abcIntArr, 'I*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'I!*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'I_*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
    });
    test('i i! i_', () => {
        expect(pack(abcIntArr, 'iii')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'i!i!i!')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'i_i_i_')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
    });
    test('l! l_', () => {
        expect(pack(abcIntArr, 'l!l!l!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l!3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l!2l!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l!l!2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l!*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l_l_l_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l_3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l_2l_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l_l_2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'l_*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('q! q_', () => {
        expect(pack(abcIntArr, 'q!q!q!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q!3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q!2q!')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q!q!2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q!*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q_q_q_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q_3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q_2q_')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q_q_2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q_*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
    });
    test('j! j_', () => {
        expect(pack(abcIntArr, 'j!j!j!')).toBe('');
        expect(pack(abcIntArr, 'j_j_j_')).toBe('');
    });
    test('S< S>', () => {
        expect(pack(abcIntArr, 'S<S<S<')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S<3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S<2S<')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S<S<2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S<*')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 'S>S>S>')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 'S>3')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 'S>2S>')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 'S>S>2')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 'S>*')).toBe('\x00A\x00B\x00C');
    });
    test('L< L>', () => {
        expect(pack(abcIntArr, 'L<L<L<')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L<3')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L<2L<')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L<L<2')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L<*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'L>L>L>')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'L>3')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'L>2L>')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'L>L>2')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'L>*')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
    });
    test('Q< Q>', () => {
        expect(pack(abcIntArr, 'Q<Q<Q<')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q<3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q<2Q<')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q<Q<2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q<*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'Q>Q>Q>')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'Q>3')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'Q>2Q>')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'Q>Q>2')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'Q>*')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
    });
    test('J< J>', () => {
        expect(pack(abcIntArr, 'J<J<J<')).toBe('');
        expect(pack(abcIntArr, 'J>J>J>')).toBe('');
    });
    test('s< s>', () => {
        expect(pack(abcIntArr, 's<s<s<')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's<3')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's<2s<')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's<s<2')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's<*')).toBe('A\x00B\x00C\x00');
        expect(pack(abcIntArr, 's>s>s>')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 's>3')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 's>2s>')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 's>s>2')).toBe('\x00A\x00B\x00C');
        expect(pack(abcIntArr, 's>*')).toBe('\x00A\x00B\x00C');
    });
    test('l< l>', () => {
        expect(pack(abcIntArr, 'l<l<l<')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l<3')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l<2l<')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l<l<2')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l<*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
        expect(pack(abcIntArr, 'l>l>l>')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'l>3')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'l>2l>')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'l>l>2')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
        expect(pack(abcIntArr, 'l>*')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
    });
    test('q< q>', () => {
        expect(pack(abcIntArr, 'q<q<q<')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q<3')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q<2Q<')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q<q<2')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q<*')).toBe('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00');
        expect(pack(abcIntArr, 'q>q>q>')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'q>3')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'q>2Q>')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'q>q>2')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
        expect(pack(abcIntArr, 'q>*')).toBe('\x00\x00\x00\x00\x00\x00\x00A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C');
    });
    test('j<>', () => {
        expect(pack(abcIntArr, 'j>j>j>')).toBe('');
        expect(pack(abcIntArr, 'j<j<j<')).toBe('');
    });
    test('n', () => {
        expect(pack(abcIntArr, 'n*')).toBe('\x00A\x00B\x00C');
    });
    test('N', () => {
        expect(pack(abcIntArr, 'N*')).toBe('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C');
    });
    test('v', () => {
        expect(pack(abcIntArr, 'v*')).toBe('A\x00B\x00C\x00');
    });
    test('V', () => {
        expect(pack(abcIntArr, 'V*')).toBe('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00');
    });
    test('U', () => {
        expect(pack(abcIntArr, 'U*')).toBe('ABC');
    });
    test('w', () => {
        expect(pack(abcIntArr, 'w*')).toBe('ABC');
    });
    test('D', () => {
        expect(pack(abcIntArr, 'D*')).toBe('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@');
    });
    test('d', () => {
        expect(pack(abcIntArr, 'd*')).toBe('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@');
    });
    test('F', () => {
        expect(pack(abcIntArr, 'F*')).toBe('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B');
    });
    test('f', () => {
        expect(pack(abcIntArr, 'f*')).toBe('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B');
    });
    test('E', () => {
        expect(pack(abcIntArr, 'E*')).toBe('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@');
    });
    test('e', () => {
        expect(pack(abcIntArr, 'e*')).toBe('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B');
    });
    test('G', () => {
        expect(pack(abcIntArr, 'G*')).toBe('@P@\x00\x00\x00\x00\x00@P\x80\x00\x00\x00\x00\x00@P\xC0\x00\x00\x00\x00\x00');
    });
    test('g', () => {
        expect(pack(abcIntArr, 'g*')).toBe('B\x82\x00\x00B\x84\x00\x00B\x86\x00\x00');
    });
    test('A', () => {
        expect(pack(abcStrArr, 'AAA')).toBe('ABC');
        expect(pack(abcStrArr, 'A1A2A3')).toBe('AB C  ');
        expect(pack(abcStrArr, 'A*AA')).toBe('ABC');
    });
    test('a', () => {
        expect(pack(abcStrArr, 'aaa')).toBe('ABC');
        expect(pack(abcStrArr, 'a1a2a3')).toBe('AB\x00C\x00\x00');
        expect(pack(abcStrArr, 'a*aa')).toBe('ABC');
    });
    test('Z', () => {
        expect(pack(abcStrArr, 'ZZZ')).toBe('ABC');
        expect(pack(abcStrArr, 'Z1Z2Z3')).toBe('AB\x00C\x00\x00');
        expect(pack(abcStrArr, 'Z*ZZ')).toBe('A\x00BC');
    });
    test('B', () => {
        expect(pack(abcStrArr, 'BBB')).toBe('\x80\x00\x80');
        expect(pack(abcStrArr, 'B*')).toBe('\x80');
    });
    test('b', () => {
        expect(pack(abcStrArr, 'bbb')).toBe('\x01\x00\x01');
        expect(pack(abcStrArr, 'b*')).toBe('\x01');
    });
    test('H', () => {
        expect(pack(abcStrArr, 'HHH')).toBe('\xA0\xB0\xC0');
        expect(pack(abcStrArr, 'H*')).toBe('\xA0');
    });
    test('h', () => {
        expect(pack(abcStrArr, 'hhh')).toBe('\n\v\f');
        expect(pack(abcStrArr, 'h*')).toBe('\n');
    });
    test('u', () => {
        expect(pack(abcStrArr, 'uuu')).toBe('!00``\n!0@``\n!0P``\n');
    });
    test('M', () => {
        expect(pack(abcStrArr, 'MMM')).toBe('A=\nB=\nC=\n');
    });
    test('m', () => {
        expect(pack(abcStrArr, 'mmm')).toBe('QQ==\nQg==\nQw==\n');
    });
    test('P', () => {
        expect(pack(abcStrArr, 'PPP')).toBe('\x10\b\xD4@R\x7F\x00\x00\xE8\a\xD4@R\x7F\x00\x00\xC0\a\xD4@R\x7F\x00\x00');
    });
    test('p', () => {
        expect(pack(abcStrArr, 'ppp')).toBe(' \x88\xDF@R\x7F\x00\x00\xA8\x87\xDF@R\x7F\x00\x00\x80\x87\xDF@R\x7F\x00\x00');
    });
    test('@', () => {
        expect(pack(abcStrArr, '@')).toBe('\x00');
        expect(pack(abcStrArr, 'A@')).toBe('A');
        expect(pack(abcStrArr, '@AAA')).toBe('\x00ABC');
        expect(pack(abcIntArr, '@')).toBe('\x00');
        expect(pack(abcIntArr, 'C@')).toBe('A');
        expect(pack(abcIntArr, '@CCC')).toBe('\x00ABC');
    });
    test('X', () => {
        expect(pack(abcStrArr, 'AXAA')).toBe('BC');
    });
    test('x', () => {
        expect(pack([], 'xxx')).toBe('\x00\x00\x00');
    });
    test('Unknown pack directive warning', () => {
        expect(pack(abcIntArr, 'Y3')).toBe('');
    });
    test('ArgumentError', () => {
        expect(() => pack(abcIntArr, 'C4')).toThrow(argErr);
    });
});
describe('unpack', () => {
    test('C', () => {
        expect(unpack('ABC', 'CCC')).toEqual(abcIntArr);
    });
    test('S', () => {
        expect(unpack('A\x00B\x00C\x00', 'SSS')).toEqual(abcIntArr);
    });
    test('L', () => {
        expect(unpack('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00', 'LLL')).toEqual(abcIntArr);
    });
    test('Q', () => {
        expect(unpack('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00', 'QQQ')).toEqual(abcIntArr);
    });
    test('J', () => {
        expect(unpack('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00', 'JJJ')).toEqual(abcIntArr);
    });
    test('c', () => {
        expect(unpack('ABC', 'ccc')).toEqual(abcIntArr);
    });
    test('s', () => {
        expect(unpack('A\x00B\x00C\x00', 'sss')).toEqual(abcIntArr);
    });
    test('l', () => {
        expect(unpack('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00', 'lll')).toEqual(abcIntArr);
    });
    test('q', () => {
        expect(unpack('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00', 'qqq')).toEqual(abcIntArr);
    });
    test('j', () => {
        expect(unpack('A\x00\x00\x00\x00\x00\x00\x00B\x00\x00\x00\x00\x00\x00\x00C\x00\x00\x00\x00\x00\x00\x00', 'jjj')).toEqual(abcIntArr);
    });
    test('i', () => {
        expect(unpack('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00', 'iii')).toEqual(abcIntArr);
    });
    test('I', () => {
        expect(unpack('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00', 'III')).toEqual(abcIntArr);
    });
    test('n', () => {
        expect(unpack('\x00A\x00B\x00C', 'nnn')).toEqual(abcIntArr);
    });
    test('N', () => {
        expect(unpack('\x00\x00\x00A\x00\x00\x00B\x00\x00\x00C', 'NNN')).toEqual(abcIntArr);
    });
    test('v', () => {
        expect(unpack('A\x00B\x00C\x00', 'vvv')).toEqual(abcIntArr);
    });
    test('V', () => {
        expect(unpack('A\x00\x00\x00B\x00\x00\x00C\x00\x00\x00', 'VVV')).toEqual(abcIntArr);
    });
    test('U', () => {
        expect(unpack('ABC', 'UUU')).toEqual(abcIntArr);
    });
    test('w', () => {
        expect(unpack('ABC', 'www')).toEqual(abcIntArr);
    });
    test('D', () => {
        expect(unpack('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@', 'DDD')).toEqual(abcIntArr);
    });
    test('d', () => {
        expect(unpack('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@', 'ddd')).toEqual(abcIntArr);
    });
    test('F', () => {
        expect(unpack('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B', 'FFF')).toEqual(abcIntArr);
    });
    test('f', () => {
        expect(unpack('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B', 'fff')).toEqual(abcIntArr);
    });
    test('E', () => {
        expect(unpack('\x00\x00\x00\x00\x00@P@\x00\x00\x00\x00\x00\x80P@\x00\x00\x00\x00\x00\xC0P@', 'EEE')).toEqual(abcIntArr);
    });
    test('e', () => {
        expect(unpack('\x00\x00\x82B\x00\x00\x84B\x00\x00\x86B', 'eee')).toEqual(abcIntArr);
    });
    test('G', () => {
        expect(unpack('@P@\x00\x00\x00\x00\x00@P\x80\x00\x00\x00\x00\x00@P\xC0\x00\x00\x00\x00\x00', 'GGG')).toEqual(abcIntArr);
    });
    test('g', () => {
        expect(unpack('B\x82\x00\x00B\x84\x00\x00B\x86\x00\x00', 'ggg')).toEqual(abcIntArr);
    });
    test('A', () => {
        expect(unpack('ABC', 'AAA')).toEqual(abcStrArr);
    });
    test('a', () => {
        expect(unpack('ABC', 'aaa')).toEqual(abcStrArr);
    });
    test('Z', () => {
        expect(unpack('ABC', 'ZZZ')).toEqual(abcStrArr);
    });
    test('B', () => {
        expect(unpack('\x80\x00\x80', 'BBB')).toEqual(abcStrArr);
    });
    test('b', () => {
        expect(unpack('\x01\x00\x01', 'bbb')).toEqual(abcStrArr);
    });
    test('H', () => {
        expect(unpack('\xA0\xB0\xC0', 'HHH')).toEqual(abcStrArr);
    });
    test('h', () => {
        expect(unpack('\n\v\f', 'hhh')).toEqual(abcStrArr);
    });
    test('u', () => {
        expect(unpack('!00``\n!0@``\n!0P``\n', 'uuu')).toEqual(abcStrArr);
    });
    test('M', () => {
        expect(unpack('A=\nB=\nC=\n', 'MMM')).toEqual(abcStrArr);
    });
    test('m', () => {
        expect(unpack('QQ==\nQg==\nQw==\n', 'mmm')).toEqual(abcStrArr);
    });
    test('P', () => {
        expect(unpack('\xD8>0\xE7A\x7F\x00\x00\x88>0\xE7A\x7F\x00\x00`>0\xE7A\x7F\x00\x00', 'PPP')).toEqual(abcStrArr);
    });
    test('p', () => {
        expect(unpack('\xA0\xC2\xAD\xEBA\x7F\x00\x00x\xC2\xAD\xEBA\x7F\x00\x00P\xC2\xAD\xEBA\x7F\x00\x00', 'ppp')).toEqual(abcStrArr);
    });
    test('@', () => {
        expect(unpack('AAA', 'AA@AA')).toEqual(["A", "B", "A", "B"]);
    });
    test('X', () => {
        expect(unpack('AAA', 'AA@AA')).toEqual(["A", "B", "B", "C"]);
    });
    test('x', () => {
        expect(unpack('ABC', 'AxA')).toEqual(["A", "C"]);
    });
    test('Unknown pack directive warning', () => {
        expect(unpack('ABC', 'Y3')).toEqual([]);
    });
    test('ArgumentError', () => {
        expect(() => unpack('ABC', 'C4')).toThrow(argErr);
    });
});
